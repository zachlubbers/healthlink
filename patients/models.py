from django.db import models

# Create your models here.

class Patient(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    DOB = models.DateField
    medical_record_number = models.IntegerField
    status = models.CharField(max_length=50, null=True)
    notes = models.TextField
    next_appointment = models.DateTimeField
    labs_due = models.DateField
    end_of_care = models.DateField
    lab_request_status = models.BooleanField(null=True)

class Appointment(models.Model):
    date = models.DateField
    time = models.TimeField
    doctor = models.CharField(max_length=200, null=True)

    patient = models.ForeignKey(
        Patient,
        related_name='appointment',
        on_delete=models.CASCADE
    )
