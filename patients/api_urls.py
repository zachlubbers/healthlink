from django.urls import path
from .views import api_list_patients

urlpatterns = [
    path("patients/", api_list_patients, name="api_list_patients")
]
