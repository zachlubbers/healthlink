from django.shortcuts import render
from django.http import JsonResponse
from .models import Patient, Appointment
from django.views.decorators.http import require_http_methods
import json
from .common.json import ModelEncoder

class PatientEncoder(ModelEncoder):
    model = Patient
    properties = [
        "id",
        "first_name",
        "last_name",
        "DOB",
        "medical_record_number",
        "status",
        "notes",
        "next_appointment",
        "labs_due",
        "end_of_care",
        "lab_request_status",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date",
        "time",
        "doctor",
        "patient",
    ]

@require_http_methods(["GET", "POST"])
def api_list_patients(request):
    if request.method == "GET":
        patients = Patient.objects.all()
        return JsonResponse(
            {"patients": patients},
            encoder = PatientEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        patient = Patient.objects.create(**content)
        return JsonResponse(
            patient,
            encoder=PatientEncoder,
            safe=False,
        )
